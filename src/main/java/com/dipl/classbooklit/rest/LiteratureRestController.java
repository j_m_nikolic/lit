package com.dipl.classbooklit.rest;

import com.dipl.classbooklit.dto.literature.LiteratureDTO;
import com.dipl.classbooklit.entity.LiteratureType;
import com.dipl.classbooklit.service.literature.LiteratureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class LiteratureRestController<DOMAIN extends LiteratureType<DTO>, DTO extends LiteratureDTO<DOMAIN, DTO>, SERVICE extends LiteratureService<DTO>> {

    private SERVICE literatureService;

    @Autowired
    public LiteratureRestController(@Qualifier("literatureServiceImpl") SERVICE literatureService) {
        this.literatureService = literatureService;
    }

    @GetMapping("/scripts")
    public List<DTO> findAll() {
        return literatureService.findAll();
    }

    @GetMapping("/scripts/{id}")
    public DTO findById(@PathVariable Long id) {
        DTO literatureDTO = literatureService.findById(id);
        if (literatureDTO == null) {
            throw new RuntimeException("There's no literature with id "+ id);
        }
        return literatureDTO;
    }

    @PostMapping("/scripts")
    public DTO addLiteratureDTO(@RequestBody DTO literatureDTO) {
        literatureDTO.setId((long) 0);
        literatureService.save(literatureDTO);
        return literatureDTO;
    }

    @PutMapping("/scripts")
    public DTO updateLiteratureDTO(@RequestBody DTO literatureDTO) {
        literatureService.save(literatureDTO);
        return literatureDTO;
    }

    @DeleteMapping("/scripts/{id}")
    public String deleteById(@PathVariable Long id) {
        DTO literatureDTO = findById(id);
        if (literatureDTO == null) {
            throw new RuntimeException("There's no literature with id "+ id);
        }
        literatureService.deleteById(id);
        return "Deleted literature with id: " + id;
    }

}
