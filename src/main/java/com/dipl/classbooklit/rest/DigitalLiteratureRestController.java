package com.dipl.classbooklit.rest;

import com.dipl.classbooklit.dto.literature.DigitalLiteratureDTO;
import com.dipl.classbooklit.entity.DigitalLiterature;
import com.dipl.classbooklit.service.literature.DigitalLiteratureService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/digital")
public class DigitalLiteratureRestController extends LiteratureRestController<DigitalLiterature, DigitalLiteratureDTO, DigitalLiteratureService> {

    public DigitalLiteratureRestController(DigitalLiteratureService digitalLiteratureService) {
        super(digitalLiteratureService);
    }

//    @GetMapping("/scripts/{id}")
//    public DigitalLiteratureDTO findById(@PathVariable Long id) {
//        DigitalLiteratureDTO digitalLiterature = digitalLiteratureService.findById(id);
//        if (digitalLiterature == null) {
//            throw new RuntimeException("There's no script with id: " + id);
//        }
//        return digitalLiterature;
//    }
//
//    @PostMapping("/scripts")
//    public DigitalLiteratureDTO addScript(@RequestBody DigitalLiteratureDTO digitalLiterature) {
//        digitalLiterature.setId((long) 0);
//        digitalLiteratureService.save(digitalLiterature);
//        return digitalLiterature;
//    }
//
//    @PutMapping("/scripts")
//    public DigitalLiteratureDTO updateScript(@RequestBody DigitalLiteratureDTO digitalLiterature) {
//        digitalLiteratureService.save(digitalLiterature);
//        return digitalLiterature;
//    }
//
//    @DeleteMapping("/scripts/{id}")
//    private String deleteById(@PathVariable Long id) {
//        DigitalLiteratureDTO digitalLiterature = findById(id);
//        if (digitalLiterature == null) {
//            throw new RuntimeException("There's no script with id: " + id);
//        }
//        digitalLiteratureService.deleteById(id);
//        return "Deleted script with id: " + id;
//    }
}
