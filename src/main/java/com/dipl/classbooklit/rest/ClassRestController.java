package com.dipl.classbooklit.rest;

import com.dipl.classbooklit.dto.classes.ClassDTO;
import com.dipl.classbooklit.dto.classes.ClassDTOWithFaculty;
import com.dipl.classbooklit.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ClassRestController {

    private ClassService classService;

    @Autowired
    public ClassRestController(ClassService classService) {
        this.classService = classService;
    }

    @GetMapping("/classes")
    public List<ClassDTOWithFaculty> findAllClassDTOWithFaculties() {
        return classService.findAllWithFaculties();
    }

    @GetMapping("/classes/{id}")
    public ClassDTOWithFaculty findById(@PathVariable Long id) {
        ClassDTOWithFaculty theClass = classService.findByIdWithFaculties(id);
        if( theClass == null) {
            throw new RuntimeException("There's no class with id: " + id);
        }
        return theClass;
    }

    @PostMapping("/classes")
    public ClassDTO addClass(@RequestBody ClassDTO theClass) {
        theClass.setId((long) 0);
        classService.save(theClass);
        return theClass;
    }

    @PutMapping("/classes")
    public ClassDTO updateClass(@RequestBody ClassDTO theClass) {
        classService.save(theClass);
        return theClass;
    }

    @DeleteMapping("/classes/{id}")
    public String deleteById(@PathVariable Long id) {
        ClassDTOWithFaculty theClass = findById(id);
        if( theClass == null) {
            throw new RuntimeException("There's no class with id: " + id);
        }
        classService.deleteById(id);
        return "Deleted class with id: " + id;
    }
}
