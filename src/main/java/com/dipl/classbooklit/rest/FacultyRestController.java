package com.dipl.classbooklit.rest;

import com.dipl.classbooklit.dto.faculty.FacultyDTOBase;
import com.dipl.classbooklit.dto.faculty.FacultyDTOWithClasses;
import com.dipl.classbooklit.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FacultyRestController {

    private FacultyService facultyService;

    @Autowired
    public FacultyRestController(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @GetMapping("/faculties")
    public List<FacultyDTOWithClasses> findAll() {
        return facultyService.findAllWithClasses();
    }

    @GetMapping("/faculties/{id}")
    public FacultyDTOWithClasses findByIdWithClasses(@PathVariable Long id) {
        FacultyDTOWithClasses faculty = facultyService.findByIdWithClasses(id);
        if (faculty == null) {
            throw new RuntimeException("There's no faculty with id: " + id);
        }
        return faculty;
    }

    @PostMapping("/faculties")
    public FacultyDTOBase addFaculty(@RequestBody FacultyDTOBase faculty) {
        faculty.setId((long) 0);
        facultyService.save(faculty);
        return faculty;
    }

    @PutMapping("/faculties")
    public FacultyDTOBase updateFaculty(@RequestBody FacultyDTOBase faculty) {
        facultyService.save(faculty);
        return faculty;
    }

    @DeleteMapping("faculties/{id}")
    public String deleteFaculty(@PathVariable Long id) {
        FacultyDTOWithClasses facultyDTO = findByIdWithClasses(id);
        if (facultyDTO == null) {
            throw new RuntimeException("There's no faculty with id: " + id);
        }
        facultyService.deleteById(id);
        return "Deleted faculty with id: " + id;
    }
}
