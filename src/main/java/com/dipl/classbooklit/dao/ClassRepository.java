package com.dipl.classbooklit.dao;

import com.dipl.classbooklit.entity.Class;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassRepository extends JpaRepository<Class, Long> {
}
