package com.dipl.classbooklit.dao;

import com.dipl.classbooklit.entity.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacultyRepository extends JpaRepository<Faculty, Long> {
}
