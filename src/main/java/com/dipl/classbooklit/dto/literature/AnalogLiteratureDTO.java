package com.dipl.classbooklit.dto.literature;

import com.dipl.classbooklit.dto.classes.ClassDTO;
import com.dipl.classbooklit.entity.AnalogLiterature;

import java.util.stream.Collectors;

public class AnalogLiteratureDTO extends LiteratureDTO<AnalogLiterature, AnalogLiteratureDTO> {

    //fields
    private String publisher;
    private String placeOfPublishing;

    //constructors
    public AnalogLiteratureDTO() {
        super();
    }

    public AnalogLiteratureDTO(AnalogLiterature analogLiterature) {
        super(analogLiterature);
        this.publisher = analogLiterature.getPublisher();
        this.placeOfPublishing = analogLiterature.getPlaceOfPublishing();
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPlaceOfPublishing() {
        return placeOfPublishing;
    }

    public void setPlaceOfPublishing(String placeOfPublishing) {
        this.placeOfPublishing = placeOfPublishing;
    }

    @Override
    public AnalogLiterature convertToDomainEntity() {
        AnalogLiterature analogLiterature = new AnalogLiterature();
        analogLiterature.setId(getId());
        analogLiterature.setName(getName());
        analogLiterature.setAuthor(getAuthor());
        analogLiterature.setYearOfPublishing(getYearOfPublishing());
        analogLiterature.setClasses(getClasses().stream().map(ClassDTO::convertToDomainEntity).collect(Collectors.toList()));
        analogLiterature.setPublisher(getPublisher());
        analogLiterature.setPlaceOfPublishing(getPlaceOfPublishing());
        return analogLiterature;
    }
}
