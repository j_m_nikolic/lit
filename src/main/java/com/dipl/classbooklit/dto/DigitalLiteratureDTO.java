package com.dipl.classbooklit.dto;

import com.dipl.classbooklit.dto.classes.ClassDTO;
import com.dipl.classbooklit.entity.DigitalLiterature;

import java.util.stream.Collectors;

public class DigitalLiteratureDTO extends LiteratureDTO<DigitalLiterature, DigitalLiteratureDTO> {

    //fields
    private String url;
    private String format;

    //controllers
    public DigitalLiteratureDTO() {
        super();
    }

    public DigitalLiteratureDTO(DigitalLiterature digitalLiterature) {
        super(digitalLiterature);
        this.url = digitalLiterature.getUrl();
        this.format = digitalLiterature.getFormat();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public DigitalLiterature convertToDomainEntity() {
        DigitalLiterature digitalLiterature = new DigitalLiterature();
        digitalLiterature.setId(getId());
        digitalLiterature.setName(getName());
        digitalLiterature.setAuthor(getAuthor());
        digitalLiterature.setYearOfPublishing(getYearOfPublishing());
        digitalLiterature.setClasses(getClasses().stream().map(ClassDTO::convertToDomainEntity).collect(Collectors.toList()));
        digitalLiterature.setUrl(getUrl());
        digitalLiterature.setFormat(getFormat());
        return digitalLiterature;
    }
}
