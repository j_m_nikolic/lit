package com.dipl.classbooklit.dto.user;

import com.dipl.classbooklit.entity.SystemUser;
import com.dipl.classbooklit.util.entityConverter;

public class SystemUserDTO implements entityConverter<SystemUser> {

    private String username;
    private String password;

    public SystemUserDTO() {
    }

    public SystemUserDTO(SystemUser user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public SystemUser convertToDomainEntity() {
        SystemUser user = new SystemUser();
        user.setUsername(getUsername());
        user.setPassword(getPassword());
        return user;
    }
}
