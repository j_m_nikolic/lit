package com.dipl.classbooklit.dto;

import com.dipl.classbooklit.dto.classes.ClassDTO;
import com.dipl.classbooklit.entity.Class;
import com.dipl.classbooklit.entity.LiteratureType;
import com.dipl.classbooklit.util.entityConverter;

import java.util.List;
import java.util.stream.Collectors;

public abstract class LiteratureDTO<DOMAIN, DTO> implements entityConverter<DOMAIN> {

    private Long id;
    private String name;
    private String author;
    private String yearOfPublishing;
    private List<ClassDTO> classes;

    LiteratureDTO(LiteratureType<DTO> literatureType) {
        this.id = literatureType.getId();
        this.name = literatureType.getName();
        this.author = literatureType.getAuthor();
        this.yearOfPublishing = literatureType.getYearOfPublishing();
        this.classes = literatureType.getClasses().stream().map(Class::convertToBaseDTO).collect(Collectors.toList());
    }

    public LiteratureDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(String yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public List<ClassDTO> getClasses() {
        return classes;
    }

    public void setClasses(List<ClassDTO> classes) {
        this.classes = classes;
    }

    @Override
    public abstract DOMAIN convertToDomainEntity();
}
