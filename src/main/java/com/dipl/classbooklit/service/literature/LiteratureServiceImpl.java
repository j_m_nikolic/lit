package com.dipl.classbooklit.service.literature;

import com.dipl.classbooklit.dao.LiteratureRepository;
import com.dipl.classbooklit.dto.literature.LiteratureDTO;
import com.dipl.classbooklit.entity.LiteratureType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public abstract class LiteratureServiceImpl<DOMAIN extends LiteratureType<DTO>, DTO extends LiteratureDTO<DOMAIN, DTO>, REPOSITORY extends LiteratureRepository<DOMAIN>> implements LiteratureService<DTO> {

    private final REPOSITORY literatureRepository;

    public LiteratureServiceImpl(REPOSITORY literatureRepository) {
        this.literatureRepository = literatureRepository;
    }

    @Override
    @Transactional
    public List<DTO> findAll() {
        List<DOMAIN> literatureTypes = literatureRepository.findAll();
        return literatureTypes.stream().map(DOMAIN::convertToBaseDTO).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public DTO findById(Long id) {
        Optional<DOMAIN> result = literatureRepository.findById(id);
        DTO literatureDTO;
        if(result.isPresent()) {
            literatureDTO = result.get().convertToBaseDTO();
        } else {
            // there's no literature with this ID
            throw new RuntimeException("There's no literature with the id: " + id);
        }
        return literatureDTO;
    }

    @Override
    @Transactional
    public void save(DTO literatureDTO) {
        DOMAIN literatureType = literatureDTO.convertToDomainEntity();
        literatureRepository.save(literatureType);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        literatureRepository.deleteById(id);
    }
}
