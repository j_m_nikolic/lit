package com.dipl.classbooklit.entity;

import com.dipl.classbooklit.dto.literature.DigitalLiteratureDTO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "digital")
public class DigitalLiterature extends LiteratureType<DigitalLiteratureDTO> {

    //fields
    @Column(name = "url")
    private String url;

    @Column(name = "format")
    private String format;

    //Constructors
    public DigitalLiterature() {
    }

    @Autowired
    public DigitalLiterature(String name, String author, String yearOfPublishing, String url, String format) {
        super.setName(name);
        super.setAuthor(author);
        super.setYearOfPublishing(yearOfPublishing);
        this.url = url;
        this.format = format;
    }

    //getters and setters

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    //method override

    @Override
    public String toString() {
        return super.toString() + "DigitalLiterature{" +
                "url='" + url + '\'' +
                ", format='" + format + '\'' +
                '}';
    }

    @Override
    public DigitalLiteratureDTO convertToBaseDTO() {
        return new DigitalLiteratureDTO(this);
    }
}
