package com.dipl.classbooklit.entity;

import com.dipl.classbooklit.dto.user.SystemUserDTO;
import com.dipl.classbooklit.util.baseDTOMapper;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "system_user")
public class SystemUser implements Serializable, baseDTOMapper<SystemUserDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String username;
    @Column
    @JsonIgnore
    private String password;

    public SystemUser() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public SystemUserDTO convertToBaseDTO() {
        return new SystemUserDTO(this);
    }
}
