package com.dipl.classbooklit.entity;

import com.dipl.classbooklit.dto.literature.AnalogLiteratureDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "analog")
public class AnalogLiterature extends LiteratureType<AnalogLiteratureDTO> {

    //fields
    @Column(name = "publisher")
    private String publisher;

    @Column(name = "place_of_publishing")
    private String placeOfPublishing;

    //constructors
    public AnalogLiterature() {
    }

    public AnalogLiterature(String name, String author, String yearOfPublishing, String publisher, String placeOfPublishing) {
        this.setName(name);
        this.setAuthor(author);
        this.setYearOfPublishing(yearOfPublishing);
        this.publisher = publisher;
        this.placeOfPublishing = placeOfPublishing;
    }

    //getters and setters
    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPlaceOfPublishing() {
        return placeOfPublishing;
    }

    public void setPlaceOfPublishing(String placeOfPublishing) {
        this.placeOfPublishing = placeOfPublishing;
    }

    //methods override
    @Override
    public String toString() {
        return super.toString() + "AnalogLiterature{" +
                "publisher='" + publisher + '\'' +
                ", placeOfPublishing='" + placeOfPublishing + '\'' +
                '}';
    }

    @Override
    public AnalogLiteratureDTO convertToBaseDTO() {
        return new AnalogLiteratureDTO(this);
    }
}
